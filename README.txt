# Ubuntu WSL scripts

This repository contains a set of scripts designed to be used in Ubuntu under
Windows Subsystem for Linux (WSL). You can execute them installing WSL in Windows 10
Fall Creators Update or above, and downloading Ubuntu from Windows Store

## Prevent export PATH into WSL console

You can use this in the .bashrc file to prevent Windows PATH from being exported under Linux:

```
PATH=$(/usr/bin/printenv PATH | /usr/bin/perl -ne 'print join(":", grep { !/\/mnt\/[a-z]/ } split(/:/));')
```

This is useful for installing programs with `apt` that are also in Windows, such as `nodejs`.

## Run Windows system folder programs

To be able to run programs from the Windows folder, you can add it to the path:

```
PATH=${PATH}:/mnt/c/Windows
```

replacing `/mnt/c` with `/mnt/[letter]`

## Run Visual Studio Code

Visual Studio Code is installed by default in the following path:

```
C:\Users\[your_user]\AppData\Local\Programs\Microsoft VS Code\
```

You can add this to your PATH environment variable to be able to execute VSCode from Ubuntu terminal:

~/.bashrc
```
PATH=${PATH}:/mnt/c/Windows:/mnt/c/Users/ferse/AppData/Local/Programs/Microsoft\ VS\ Code
```

```
$ Code.exe
```