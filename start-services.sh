#!/bin/bash

VERSION=$(lsb_release -a | grep Release: | awk '/Release/{print $2}')

if ! pidof apache2 > /dev/null
then
    sudo apachectl start

    if [ $VERSION == "18.04" ]; then
        sudo service mysql start
        sudo service mysql stop
        sudo /usr/sbin/mysqld --skip-grant-tables &
    elif [ $VERSION == "16.04" ]; then
        sudo service mysql start
    fi

    #sudo service mongodb start
fi