#!/bin/bash

VERSION=$(lsb_release -a | grep Release: | awk '/Release/{print $2}')

if [ "$#" -ne 1 ]; then
    echo "Usage: install-lamp.sh [public http directory path]"
    exit
fi

WWW=$1

if grep -q Microsoft /proc/version; then
    echo "WSL (Windows Subsystem for Linux)"
    if [ $VERSION == "18.04" ]; then
        echo "Ubuntu versión " ${VERSION}
        sudo apt update
        sudo sudo apt-get install -y mysql-server php-mysql apache2 php libapache2-mod-php php-mbstring php7.2-mbstring php-gettext php7.2-zip build-essential
        sudo a2enmod proxy
        sudo a2enmod rewrite
        sudo cp 000-default.conf /etc/apache2/sites-enabled

        # NOTE: for Windows WSL installations, mysql doesn't connect using the default service
        # and it's necessary to disable security. USE ONLY ON DEVELOPMENT ENVIRONEMNTS
        # or install the native MySQL windows version
        sudo /usr/sbin/mysqld --skip-grant-tables &
        sudo apachectl start

        sudo rm -rf /var/www/html
        sudo ln -s "${WWW}" /var/www/html

        sudo mysql_secure_installation

    elif [ $VERSION == "16.04" ]; then
        echo "Ubuntu version " ${VERSION}
        sudo apt-get update 
        sudo apt-get install -y mysql-server php-mysql apache2 php libapache2-mod-php php-mbstring php7.0-mbstring php-gettext php7.0-zip build-essential
        sudo a2enmod proxy
        sudo a2enmod rewrite
        sudo cp 000-default.conf /etc/apache2/sites-enabled
        sudo cp php.ini /etc/php/7.0/apache2


        #
        #curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
        #sudo apt-get install -y nodejs
        #
        sudo service mysql start
        sudo apachectl start

        sudo rm -rf /var/www/html
        sudo ln -s "${WWW}" /var/www/html
        #
        #sudo npm install -g gulp
        #sudo npm install -g bower

    else
        echo "Distribution version not compatible"
    fi

else
  echo "Native linux"
  sudo apt-get update
  sudo apt-get install -y mysql-server php-mysql apache2 php libapache2-mod-php php-mbstring php7.2-mbstring php-gettext php7.2-zip build-essential
  sudo a2enmod proxy
  sudo a2enmod rewrite
  sudo cp 000-default.conf /etc/apache2/sites-enabled
  sudo cp php.ini /etc/php/7.2/apache2
  sudo apachectl restart

  sudo rm -rf /var/www/html
  sudo ln -s "${WWW}" /var/www/html

  sudo mysql_secure_installation
fi

